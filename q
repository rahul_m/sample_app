[1mdiff --git a/app/helpers/application_helper.rb b/app/helpers/application_helper.rb[m
[1mindex de6be79..be92c56 100644[m
[1m--- a/app/helpers/application_helper.rb[m
[1m+++ b/app/helpers/application_helper.rb[m
[36m@@ -1,2 +1,13 @@[m
 module ApplicationHelper[m
[32m+[m[41m  [m
[32m+[m[32m  # return the full title[m
[32m+[m[32m  def full_title(page_title = '')[m
[32m+[m[32m    base_title = "Ruby on Rails Tutorial Sample App"[m
[32m+[m[41m    [m
[32m+[m[32m    if page_title.empty?[m
[32m+[m[32m      base_title[m
[32m+[m[32m    else[m
[32m+[m[32m      page_title + " | " + base_title[m
[32m+[m[32m    end[m
[32m+[m[32m  end[m
 end[m
[1mdiff --git a/app/views/layouts/application.html.erb b/app/views/layouts/application.html.erb[m
[1mindex 72e5ddd..c6854f1 100644[m
[1m--- a/app/views/layouts/application.html.erb[m
[1m+++ b/app/views/layouts/application.html.erb[m
[36m@@ -1,7 +1,7 @@[m
 <!DOCTYPE html>[m
 <html>[m
   <head>[m
[31m-    <title><%= yield(:title) %> | Ruby on Rails Tutorial Sample App</title>[m
[32m+[m[32m    <title><%= full_title yield(:title) %></title>[m
     <%= csrf_meta_tags %>[m
 [m
     <%= stylesheet_link_tag    'application', media: 'all', [m
