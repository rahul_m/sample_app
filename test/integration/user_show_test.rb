require 'test_helper'

class UserShowTest < ActionDispatch::IntegrationTest
  def setup
    @inactive_user  = users(:inactive)
    @activated_user = users(:lana)
  end
  
  test "redirect for not activated_user" do 
    log_in_as(@inactive_user)
    assert_redirected_to root_url
  end
  
  test "show profile for activated_user" do 
    log_in_as(@activated_user)
    assert_redirected_to user_url(@activated_user)
  end

end
